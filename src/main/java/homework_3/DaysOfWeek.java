package homework_3;

public enum DaysOfWeek {
    MONDAY("monday"),
    TUESDAY("tuesday"),
    WEDNESDAY("wednesday"),
    THURSDAY("thursday"),
    FRIDAY("friday"),
    SATURDAY("saturday"),
    SUNDAY("sunday");
    private String day;

    DaysOfWeek(String code) {
        this.day = code;
    }

    public String getVal() {
        return day;
    }
}
