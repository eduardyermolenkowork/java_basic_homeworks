package homework_1;


import java.util.*;

public class Main {
    public static int[] updateAndCreateArr(int[] numbers, int inputNumber) {
        int[] numbersTmp = new int[numbers.length + 1];
        for (int i = 0; i < numbers.length; i++) {
            numbersTmp[i] = numbers[i];
        }
        numbersTmp[numbersTmp.length - 1] = inputNumber;
        return numbersTmp;
    }
    public static int[] quickSort(int[] arr){
        if(arr.length == 0) return arr;
        int[] pivot = {arr[arr.length - 1]};
        int[] leftSubArr = filterArr(arr, pivot[0], (p, a) -> a < p);
        int[] rightSubArr = filterArr(arr, pivot[0], (p, a) -> a > p);
        return mergeArr(quickSort(leftSubArr), pivot, quickSort(rightSubArr));
    }
    private static int[] mergeArr(int[] ...arrays){
        int finalLenght = 0;
        for (int i = 0; i < arrays.length; i++) {
            for (int j = 0; j < arrays[i].length; j++) {
                finalLenght++;
            }
        }
        int[] finalArr = new int[finalLenght];
        int indexOfSubArr = 0;
        int j = 0;
        for (int i = 0; i < finalLenght; i++) {
            if (j == arrays[indexOfSubArr].length){
                indexOfSubArr++;
                j = 0;
            }
            finalArr[i] = arrays[indexOfSubArr][j++];
        }
        return finalArr;
    }
    private static int[] filterArr(int[] arr, int pivot, biPredicate<Integer, Integer> bp){
        int[] halfArr = new int[0];
        for (int i = 0; i < arr.length; i++) {
            if (bp.test(pivot, arr[i])) {
                halfArr = updateAndCreateArr(halfArr, arr[i]);
            }
        }
        return halfArr;
    }

    public interface biPredicate<T, R>{
        boolean test(T t, R r);
    }
    public static void main(String[] args) {
        int randomNumber = new Random().nextInt(100);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Hola! Enter your name");
        String name = scanner.next();
        System.out.println("Let the game begin!");
        int[] numbers = new int[0];
        while (true) {
            System.out.println("Enter the number");
            String inputVal = scanner.next();
            int inputNumber;
            try {
                inputNumber = Integer.parseInt(inputVal);
            } catch (NumberFormatException e){
                System.out.printf("Error, %s enter the decimal number\n", e.getMessage());
                continue;
            }
            if (Integer.parseInt(inputVal) >= 0 && Integer.parseInt(inputVal) <= 100) {
                inputNumber = Integer.parseInt(inputVal);
            } else {
                System.out.println("Number must be in range [0-100], try again");
                continue;
            }
            if (inputNumber == randomNumber) {
                numbers = updateAndCreateArr(numbers, inputNumber);
                System.out.printf("Congratulations, %s!\n", name);
                System.out.printf("Your numbers: %s\n", Arrays.toString(quickSort(numbers)));
                break;
            } else if (inputNumber < randomNumber) {
                System.out.println("Your number is too small. Please, try again");
                numbers = updateAndCreateArr(numbers, inputNumber);

            } else {
                System.out.println("Your number is too big. Please, try again.");
                numbers = updateAndCreateArr(numbers, inputNumber);
            }
        }
    }
}
