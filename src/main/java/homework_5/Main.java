package homework_5;

import java.util.*;

public class Main {
    public static void main(String[] args)  {
        String[][] scheduleMother = {
                {"Monday","Learning JAVA"},
                {"Tuesday","Computer science; walk; date with girl"},
                {"Wednesday", "Do homework's; lesson in DAN-IT"},
                {"Thursday", "Write science work; walk"},
                {"Friday", "lesson in DAN-IT"},
                {"Saturday", "Date with girl; drink, drink and drink more"},
                {"Sunday", "Seeping; rest; playing in computer"}
        };
        String[][] scheduleFather = {
                {"Monday", "Work at factory; drink beer"},
                {"Tuesday", "Work at factory; drink beer"},
                {"Wednesday", "Work at factory; drink beer"},
                {"Thursday", "Work at factory; drink beer"},
                {"Friday", "Work at factory; drink beer"},
                {"Saturday", "Have a rest; drink beer"},
                {"Sunday", "Have a rest; drink beer"},
        };
        Human david = new Human("David", "Anderson", 1988, 90, scheduleFather);
        Human sara = new Human("Sara", "Anderson", 1992, 70, scheduleMother);
        Human freddy = new Human("Freddy", "Anderson", 2013);
        System.out.println(freddy.getFamily());
        Pet cat = new Pet("cat", "goga");
        System.out.println(freddy);
        Family f1 = new Family(david, sara);
        System.out.println(f1);
        f1.setPet(cat);
        f1.addChild(freddy);
        System.out.println(freddy.getFamily());
        System.out.println(f1.deleteChildren(0));
        System.out.println(f1);
        System.out.println(freddy.getFamily());
        f1.addChild(freddy);
        System.out.println(f1);
        f1.deleteChildren(freddy);
        System.out.println(freddy.getFamily());
    }

}
