package homework_5;

import java.util.*;

public class Family {
    private Human father;
    private Human mother;
    private Human[] children = new Human[0];
    private Pet pet;

    public Family(Human father, Human mother) {
        this.father = father;
        this.mother = mother;
        father.setFamily(this);
        mother.setFamily(this);
    }

    public Family(Human father, Human mother, Human[] children) {
        this(father, mother);
        this.children = children;
    }

    public Family(Human father, Human mother, Human[] children, Pet pet) {
        this(father, mother, children);
        this.pet = pet;
    }

    public boolean deleteChildren(int index) {
        if (children.length - 1 < index) throw new IndexOutOfBoundsException();
        Human[] tmp = new Human[this.children.length - 1];
        int j = 0;
        for (int i = 0; i < this.children.length; i++) {
            if (i != index) {
                tmp[j++] = this.children[i];
                //this.children[i].setFamily(null);
            }
        }
        this.children = tmp;
        return true;
    }

    public boolean deleteChildren(Human child) {
        for (int i = 0; i < this.children.length; i++) {
            if (this.children[i].equals(child)) {
                child.setFamily(null);
                return deleteChildren(i);
            }
        }
        return false;
    }

    public void addChild(Human child) {
        Human[] tmp = new Human[this.children.length + 1];
        for (int i = 0; i < this.children.length; i++) {
            tmp[i] = this.children[i];
        }
        child.setFamily(this);
        tmp[tmp.length - 1] = child;
        this.children = tmp;
    }

    public int countFamily() {
        return 2 + this.children.length;
    }

    public Human getFather() {
        return father;
    }

    public Human getMother() {
        return mother;
    }

    public Human[] getChildren() {
        return children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setChildren(Human[] children) {
        this.children = children;
        for (Human c : children) {
            c.setFamily(this);
        }
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(father, family.father) &&
                Objects.equals(mother, family.mother) &&
                Arrays.equals(children, family.children) &&
                Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(father, mother, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public String toString() {
        return String.valueOf(new StringBuilder()
                .append("Family{")
                .append(String.format("father=%s", this.father))
                .append(String.format(", mother=%s", this.mother))
                .append(this.children == null ? "" : String.format(", children=%s", Arrays.toString(this.children)))
                .append(this.pet == null ? "" : String.format(", pet=%s", this.pet))
                .append("}"));
    }
}
