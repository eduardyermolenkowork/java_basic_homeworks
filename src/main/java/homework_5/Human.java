package homework_5;

import java.util.*;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private String[][] schedule;

    public Human(String name, String surname, int year, int iq, Family family, String[][] schedule){
        this(name, surname, year, iq, schedule);
        this.family = family;
    }

    public Human(String name, String surname, int year, int iq,  String[][] schedule) {
        this(name, surname, year);
        this.iq = iq;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human() {
    }

    public void greetPet(){
        try {
            System.out.println(String.format("Привет, %s", this.family.getPet().getNickname()));
        } catch (NullPointerException e){
            System.out.println("Error: Pet is Null");
        }

    }
    public void describePet(){
        try {
            System.out.println(new StringBuilder()
                    .append(String.format("У меня есть %s", family.getPet().getSpecies()))
                    .append(this.family.getPet().getAge() == 0 ? "" : String.format(", ему %s лет", family.getPet().getAge()))
                    .append(this.family.getPet().getTrickLevel() == -1 ? "" : String.format(", он %s", family.getPet().getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый"))
            );
        }catch (NullPointerException e){
            System.out.println("Error: Pet is Null");
        }
    }
    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

    public Family getFamily() {
        return family;
    }
    public void setFamily(Family family) {
        this.family = family;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    @Override
    public String toString() {
        return String.valueOf(new StringBuilder()
                .append("Human{")
                .append(this.name == null ? "": String.format("name=%s", this.name))
                .append(this.surname == null ? "": String.format(", surname=%s", this.surname))
                .append(this.year == 0 ? "": String.format(", year=%d", this.year))
                .append(this.iq == 0 ? "": String.format(", iq=%s", this.iq))
                .append(this.schedule == null ? "": String.format(", schedule=%s", Arrays.deepToString(this.schedule)))
                .append("}")
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(family, human.family) &&
                Arrays.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, iq, family);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }
}
