package homework_6;

import java.util.ArrayList;
import java.util.Objects;
import java.util.function.DoubleToIntFunction;
import java.util.function.Function;

public class Main {


    public static void main(String[] args) {
        Human h1 = new Human("Alex", "Popov", 1977);
        System.out.println(h1);
    }

    public static void main2(String[] args)  {

        while (true){
            for (int i = 0; i < 100; i++) {
                new Human("Alex", "Popov", 1977, 50, new String[][]{
                        {DayOfWeek.MONDAY.name(), "Work at factory; drink beer"},
                        {DayOfWeek.TUESDAY.name(), "Work at factory; drink beer"},
                        {DayOfWeek.WEDNESDAY.name(), "Work at factory; drink beer"},
                        {DayOfWeek.THURSDAY.name(), "Work at factory; drink beer"},
                        {DayOfWeek.FRIDAY.name(), "Work at factory; drink beer"},
                        {DayOfWeek.SATURDAY.name(), "Have a rest; drink beer"},
                        {DayOfWeek.SUNDAY.name(), "Have a rest; drink beer"},
                });
            }

            try {
                Thread.sleep(5);
            } catch (InterruptedException e){
                System.err.println(e.getMessage());
            }
            System.out.println("Total: "+Runtime.getRuntime().totalMemory()+
                    "; free: "+Runtime.getRuntime().freeMemory());

        }

    }

}
