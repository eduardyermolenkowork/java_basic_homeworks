package homework_6;

public enum Species {
    CAT(false, 4, true, "Кошачьи"),
    DOG(false, 4, true, "Псовые"),
    PARROT(true, 2, true, "Птицы"),
    FISH(false, 0, false, "Рыбы"),
    SNAKE(false, 0, false, "Рептилии");

    boolean canFly;
    int numberOfLegs;
    boolean hasFur;
    String kindOfAnimal;

    Species(boolean canFly, int numberOfLegs, boolean hasFur, String kindOfAnimal) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
        this.kindOfAnimal = kindOfAnimal;
    }
}
