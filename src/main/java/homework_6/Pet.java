package homework_6;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel = -1;
    private String[] habits;

    public void eat(){
        System.out.println("Я кушаю");
    }
    public void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n", this.nickname);
    }
    public void foul(){
        System.out.println("Нужно хорошо замести следы...");
    }

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this(species, nickname);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet() {
    }

    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

//    @Override
//    public String toString() {
//        return String.format("%s{nickname=%s, age=%d, trickLevel=%d, habits=%s}\n",
//                        this.species, this.nickname, this.age, this.trickLevel, Arrays.toString(this.habits));
//    }

    @Override
    public String toString() {
        return String.valueOf(new StringBuilder()
                .append(this.species == null ? "pet": this.species)
                .append("{")
                .append(this.nickname == null ? "": String.format("nickname=%s", this.nickname))
                .append(this.age == 0 ? "": String.format(", age=%d", this.age))
                .append(this.trickLevel == -1 ? "": String.format(", trickLevel=%d", this.trickLevel))
                .append(this.habits == null ? "": String.format(", habits=%s", Arrays.toString(this.habits)))
                .append(this.species == null ? "" : String.format(", params={can fly=%b, number of legs=%d, has fur=%b, kind of animal=%s}",
                        this.species.canFly, this.species.numberOfLegs, this.species.hasFur, this.species.kindOfAnimal))
                .append("}")
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return  age == pet.age &&
                trickLevel == pet.trickLevel &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(species, nickname, age, trickLevel);
        result = 31 * result + Arrays.hashCode(habits);
        return result;
    }

}
