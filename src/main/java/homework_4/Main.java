package homework_4;

public class Main {
    public static void main(String[] args)  {
        System.out.println("------------------------------");
        Pet rocket = new Pet("raccoon", "rocket");
        Pet goga = new Pet("rabbit", "goga", 4, 30, new String[]{"jump", "eat", "funny bath"});
        Pet pet = new Pet();
        System.out.println(rocket);
        System.out.println(goga);
        System.out.println(pet);
        goga.foul();
        goga.eat();
        goga.respond();
        System.out.println("------------------------------");
        Human grandMotherGalina = new Human("Galina", "Zhmishenko", 1937);
        Human grandFatherAlbert = new Human("Albert", "Zhmishenko", 1933);
        System.out.println(grandFatherAlbert);
        System.out.println(grandMotherGalina);
        System.out.println("------------------------------");
        String[][] scheduleOfValera = {
                {"Monday", "Work at factory; drink beer"},
                {"Tuesday", "Work at factory; drink beer"},
                {"Wednesday", "Work at factory; drink beer"},
                {"Thursday", "Work at factory; drink beer"},
                {"Friday", "Work at factory; drink beer"},
                {"Saturday", "Have a rest; drink beer"},
                {"Sunday", "Have a rest; drink beer"},
        };
        String[][] scheduleOfDenis = {
                {"Monday","Learning JAVA"},
                {"Tuesday","Computer science; walk; date with girl"},
                {"Wednesday", "Do homework's; lesson in DAN-IT"},
                {"Thursday", "Write science work; walk"},
                {"Friday", "lesson in DAN-IT"},
                {"Saturday", "Date with girl; drink, drink and drink more"},
                {"Sunday", "Seeping; rest; playing in computer"}
        };

        Human fatherValera = new Human("Valerii", "Zhmishenko", 1965, 54, rocket, grandMotherGalina, grandFatherAlbert, scheduleOfValera);
        Human motherSvetlana = new Human("Svetlana", "Zmishenko", 1971);
        Human denis = new Human("Denis", "Suxachev", 1989, 100, goga, motherSvetlana, fatherValera, scheduleOfDenis);
        Human futureChildOfDenis = new Human();
        System.out.println(fatherValera);
        System.out.println(motherSvetlana);
        System.out.println(denis);
        System.out.println(futureChildOfDenis);
        System.out.println("------------------------------");
        denis.describePet();
        fatherValera.greetPet();
        fatherValera.describePet();
        motherSvetlana.greetPet();

    }

}
