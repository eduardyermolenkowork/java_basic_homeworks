package homework_4;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private String[][] schedule;

    public Human(String name, String surname, int year, int iq, Pet pet, Human mother, Human father, String[][] schedule) {
        this(name, surname, year, mother, father);
        this.iq = iq;
        this.pet = pet;
        this.schedule = schedule;
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this(name, surname, year);
        this.mother = mother;
        this.father = father;
    }


    public Human() {
    }

    public void greetPet() {
        try {
            System.out.println(String.format("Привет, %s", this.pet.getNickname()));
        } catch (NullPointerException e) {
            System.out.println("Error: Pet is Null");
        }

    }

    public void describePet() {
        try {
            System.out.println(new StringBuilder()
                    .append(String.format("У меня есть %s", this.pet.getSpecies()))
                    .append(this.pet.getAge() == 0 ? "" : String.format(", ему %s лет", this.pet.getAge()))
                    .append(this.pet.getTrickLevel() == -1 ? "" : String.format(", он %s", this.pet.getTrickLevel() > 50 ? "очень хитрый" : "почти не хитрый"))
            );
        } catch (NullPointerException e) {
            System.out.println("Error: Pet is Null");
        }
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }

    public int getIq() {
        return iq;
    }

    public Pet getPet() {
        return pet;
    }

    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    @Override
    public String toString() {
        return String.valueOf(new StringBuilder()
                .append("Human{")
                .append(this.name == null ? "" : String.format("name=%s", this.name))
                .append(this.surname == null ? "" : String.format(", surname=%s", this.surname))
                .append(this.year == 0 ? "" : String.format(", year=%d", this.year))
                .append(this.iq == 0 ? "" : String.format(", iq=%s", this.iq))
                .append(this.mother == null ? "" : String.format(", mother=%s %s", this.mother.name, this.mother.surname))
                .append(this.father == null ? "" : String.format(", mother=%s %s", this.father.name, this.father.surname))
                .append(this.pet == null ? "" : String.format(", pet=%s", this.pet.toString()))
                .append("}")
        );
    }
}
