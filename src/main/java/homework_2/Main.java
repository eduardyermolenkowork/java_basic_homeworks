package homework_2;
import java.util.*;

public class Main {
    private static final char MISSED_TARGET = 42;
    private static final char HIT_TARGET = 120;
    private static final char VOID_CELL = 45;


    public static int[][] createBoard(int width, int height) {
        int[][] board = new int[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if ((i == 0)) {
                    board[i][j] = j;
                } else if (j == 0) {
                    board[i][j] = i;
                } else board[i][j] = VOID_CELL;
            }
        }
        return board;

    }
    public static void printBoard(int[][] board){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if(i != 0 && j != 0){
                    sb.append((char) board[i][j]);
                } else sb.append(board[i][j]);
                sb.append(" | ");
            }
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }
    public static boolean validateCoordinate(int point, int maxLength){
        return point >= 1 && point <= maxLength;
    }
    public static int scannerPoint(String nameOfPoint, int maxLength){
        int point;
        while (true) {
            try {
                System.out.printf("Enter the %s coordinate of point\n", nameOfPoint);
                point = Integer.parseInt(new Scanner(System.in).next());
                if(validateCoordinate(point, maxLength)) break;
                else {
                    System.out.printf("number must be in range [1-%d], try again\n", maxLength);
                }
            } catch (NumberFormatException e){
                System.err.println("incorrect input value, try again");
            }
        }
        return point;
    }

    public static void main(String[] args) {
        final int widthBoard = 6;
        final int heightBoard = 6;
        int targetX = (int) (Math.random() * widthBoard - 1) + 1;
        int targetY = (int) (Math.random() * heightBoard - 1) + 1;
        int[][] board = createBoard(widthBoard, heightBoard);
        int inputPointX;
        int inputPointY;
        System.out.println("All set. Get ready to rumble!");
        while (true){
            inputPointX = scannerPoint("X", heightBoard);
            inputPointY = scannerPoint("Y", widthBoard);
            if(inputPointX == targetX && inputPointY == targetY){
                board[inputPointY][inputPointX] = HIT_TARGET;
                System.out.println("You have won!");
                printBoard(board);
                break;
            } else {
                board[inputPointY][inputPointX] = MISSED_TARGET;
                printBoard(board);
            }
        }
    }
}
