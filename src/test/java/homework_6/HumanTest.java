package homework_6;

import org.junit.Test;

import static org.junit.Assert.*;

public class HumanTest {

    @Test
    public void testToString() {
        Human h = new Human();
        assertEquals( "Human{}", h.toString());
    }

    @Test
    public void testToString2() {
        Human h = new Human("Alex", "Popov", 1977);
        assertEquals("Human{name=Alex, surname=Popov, year=1977}", h.toString());
    }

    @Test
    public void testToString3() {
        Human h = new Human("Alex", "Popov", 1977, 50, new String[][]{
                {DayOfWeek.MONDAY.name(), "Work at factory; drink beer"},
                {DayOfWeek.TUESDAY.name(), "Work at factory; drink beer"},
                {DayOfWeek.WEDNESDAY.name(), "Work at factory; drink beer"},
                {DayOfWeek.THURSDAY.name(), "Work at factory; drink beer"},
                {DayOfWeek.FRIDAY.name(), "Work at factory; drink beer"},
                {DayOfWeek.SATURDAY.name(), "Have a rest; drink beer"},
                {DayOfWeek.SUNDAY.name(), "Have a rest; drink beer"},
        });
        assertEquals("Human{name=Alex, surname=Popov, year=1977, iq=50, schedule=[[MONDAY, Work at factory; drink beer], [TUESDAY, Work at factory; drink beer], [WEDNESDAY, Work at factory; drink beer], [THURSDAY, Work at factory; drink beer], [FRIDAY, Work at factory; drink beer], [SATURDAY, Have a rest; drink beer], [SUNDAY, Have a rest; drink beer]]}"
                , h.toString());
    }
}