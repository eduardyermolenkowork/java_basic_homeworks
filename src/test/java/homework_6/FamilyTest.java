package homework_6;

import org.junit.Test;

import java.util.Arrays;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class FamilyTest {

    @Test
    public void testDeleteChildren1() {
        Human[] children = {
                new Human("Vasya", "Shlyapik", 2001),
                new Human("Karina", "Shlyapik", 2003),
                new Human("Dan", "Shlyapik", 2004)
        };
        Human childInFamily = new Human("Dan", "Shlyapik", 2004);
        Family f = new Family(new Human("Alex", "Shlyapik", 1977),
                new Human("Svetlana", "Shlyapik", 1980),
                children
        );
        f.deleteChildren(childInFamily);
        assertTrue(!Arrays.asList(f.getChildren()).contains(childInFamily));
    }

    @Test
    public void testDeleteChildren2() {
        Human[] children = {
                new Human("Vasya", "Shlyapik", 2001),
                new Human("Karina", "Shlyapik", 2003),
                new Human("Dan", "Shlyapik", 2004)
        };
        Family f = new Family(new Human("Alex", "Shlyapik", 1977),
                new Human("Svetlana", "Shlyapik", 1980),
                children
        );
        Human childNotInFamily = new Human("Vitya", "Shpak", 1999);
        f.deleteChildren(childNotInFamily);
        assertEquals(Arrays.asList(f.getChildren()), Arrays.asList(children));
    }

    @Test
    public void testDeleteChildren3() {
        Human[] children = {
                new Human("Vasya", "Shlyapik", 2001),
                new Human("Karina", "Shlyapik", 2003),
                new Human("Dan", "Shlyapik", 2004)
        };
        Human deletedChild = new Human("Vasya", "Shlyapik", 2001);
        int indexDeletedChild = 0;
        Family f = new Family(new Human("Alex", "Shlyapik", 1977),
                new Human("Svetlana", "Shlyapik", 1980),
                children
        );
        assertTrue(f.deleteChildren(indexDeletedChild));
        assertTrue(!Arrays.asList(f.getChildren()).contains(deletedChild));
    }
    @Test(expected = IndexOutOfBoundsException.class)
    public void testDeleteChildren4() {
        Human[] children = {
                new Human("Vasya", "Shlyapik", 2001),
                new Human("Karina", "Shlyapik", 2003),
                new Human("Dan", "Shlyapik", 2004)
        };
        int indexOutOfArray = 4;
        Family f = new Family(new Human("Alex", "Shlyapik", 1977),
                new Human("Svetlana", "Shlyapik", 1980),
                children
        );
        assertTrue(!f.deleteChildren(indexOutOfArray));
        assertEquals(f.getChildren().length, children.length);
    }

    @Test
    public void testAddChild() {
        Human[] children = {
                new Human("Vasya", "Shlyapik", 2001),
                new Human("Karina", "Shlyapik", 2003),
                new Human("Dan", "Shlyapik", 2004)
        };
        Human newChild = new Human("Vitya", "Shpak", 1999);
        Family f = new Family(new Human("Alex", "Shlyapik", 1977),
                new Human("Svetlana", "Shlyapik", 1980),
                children
        );
        f.addChild(newChild);
        assertTrue(Arrays.asList(f.getChildren()).contains(newChild));
    }

    @Test
    public void countFamily() {
        Human[] children = {
                new Human("Vasya", "Shlyapik", 2001),
                new Human("Karina", "Shlyapik", 2003),
                new Human("Dan", "Shlyapik", 2004)
        };
        Family f = new Family(new Human("Alex", "Shlyapik", 1977),
                new Human("Svetlana", "Shlyapik", 1980),
                children
        );
        int expectedCountFamily = 5;
        assertEquals(expectedCountFamily, f.countFamily());
    }
}